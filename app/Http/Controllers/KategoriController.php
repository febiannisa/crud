<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layout'] = 'layout.web';
        $data['page'] = 'Kategori';
        $data['app'] = 'JCC';
        $data['kategori'] = DB::table('kategori')->get();
        return view('pages.kategori.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['layout'] = 'layout.web';
        $data['page'] = 'Kategori';
        $data['app'] = 'JCC';
        return view('pages.kategori.create' ,  ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'nama' => 'required',
                'deskripsi' => 'required',     
            ],
            [
                'nama.required' => 'Inputan Nama Harus Diisi',
                'deskripsi.required' => 'Deskripsi Harus Diisi',
            ]
        );

        DB::table('kategori')->insert(
            [
                'nama' => $request['nama'],
                'deskripsi' => $request['deskripsi'] 
            ]
        );

        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['layout'] = 'layout.web';
        $data['page'] = 'Kategori';
        $data['app'] = 'JCC';
        $data['kategori'] = DB::table('kategori')->where('id', $id)->first();
        return view('pages.kategori.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['layout'] = 'layout.web';
        $data['page'] = 'Kategori';
        $data['app'] = 'JCC';
        $data['kategori'] = DB::table('kategori')->where('id', $id)->first();
        return view('pages.kategori.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'deskripsi' => 'required',     
            ],
            [
                'nama.required' => 'Inputan Nama Harus Diisi',
                'deskripsi.required' => 'Deskripsi Harus Diisi',
            ]
        );

        DB::table('kategori')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'deskripsi' => $request['deskripsi'], 
                ]
            );
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kategori')->where('id', '=', $id)->delete();
        return redirect('/kategori');
    }
}
