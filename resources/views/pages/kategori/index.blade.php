@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

<a href="/kategori/create" class="btn btn-primary my-3">Tambah Kategori</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($data['kategori'] as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->deskripsi}}</td>
              <td>                  
                  <form action="/kategori/{{$item->id}}" method="POST">
                      @csrf
                      <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @method('delete')
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                  </form>
              </td>
          </tr>
      @empty
          <h1>Data Kosong</h1>
      @endforelse
    </tbody>
  </table>
@endsection