@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

    <form action="/kategori/{{$data['kategori']->id}}" method="POST">
        @csrf
        @method('put')
            <div class="form-group">
                <label>Nama Kategori</label>
                <input type="text" value="{{$data['kategori']->nama}}" class="form-control" name="nama">                
            </div>
            @error('nama')
                <div class="alert alert-danger">{{$message}}</div>                
            @enderror
            <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi Kategori</label>
                <textarea name="deskripsi" class="form-control">{{$data['kategori']->deskripsi}}</textarea>
            </div>
            @error('deskripsi')
                <div class="alert alert-danger">{{$message}}</div>                
            @enderror           
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection