@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

    <form action="/kategori" method="POST">
        @csrf
            <div class="form-group">
                <label>Nama Kategori</label>
                <input type="text" class="form-control" name="nama">                
            </div>
            @error('nama')
                <div class="alert alert-danger">{{$message}}</div>                
            @enderror
            <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi Kategori</label>
                <textarea name="deskripsi" class="form-control"></textarea>
            </div>
            @error('deskripsi')
                <div class="alert alert-danger">{{$message}}</div>                
            @enderror           
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection